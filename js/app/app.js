var app = angular.module( "app", [] );
	app.directive( "syoDirective", function () {
		return {
    		restrict: 'E',
    		template: '<div></div>',
    		replace: true,
    		link: function ( scope, element, attrs ) {
            	var latitudeLongitude = new google.maps.LatLng( -29.6836549,-51.4572224 );
            	var mapOptions = {
                center: latitudeLongitude,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var mapa = new google.maps.Map( document.getElementById( attrs.id ), mapOptions );
            var marcador = new google.maps.Marker( {
                position: latitudeLongitude,
                mapa: mapa,
                title: "Localização da Syonet"
            } );
            marcador.setMap( mapa );
        }
    };
 } );